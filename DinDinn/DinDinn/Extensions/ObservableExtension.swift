//
//  ObservableExtension.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 07/04/21.
//

import Foundation
import ObjectMapper
import RxSwift
import Moya

extension Observable {

    func mapObject<T: Mappable>(type: T.Type) -> Observable<T> {
        return self.map { response in
            //if response dictionary, use ObjectMapper to map dictionary
            //if not throw an error
            guard let dict = response as? [String: Any] else {
                throw RxSwiftMoyaError.ParseJSONError
            }
            return Mapper<T>().map(JSON: dict)!
        }
    }

    func mapArray<T: Mappable>(type: T.Type) -> Observable<[T]> {
        return self.map { response in
            //if response array, use ObjectMapper to map the array
            //if not, throw an error
            guard let array = response as? [Any] else {
                throw RxSwiftMoyaError.ParseJSONError
            }

            guard let dicts = array as? [[String: Any]] else {
                throw RxSwiftMoyaError.ParseJSONError
            }
            return Mapper<T>().mapArray(JSONArray: dicts)
        }
    }
}

enum RxSwiftMoyaError: String {
    case ParseJSONError
    case OtherError
}
extension RxSwiftMoyaError: Swift.Error { }


//extension Moya.Response {
//    func mapObject<T: Mappable>(type: T.Type) -> Observable<T> {
//        return self.map { response in
//            //if response dictionary, use ObjectMapper to map dictionary
//            //if not throw an error
//            guard let dict = response as? [String: Any] else {
//                throw RxSwiftMoyaError.ParseJSONError
//            }
//            return Mapper<T>().map(JSON: dict)!
//        }
//    }
//
//    func mapArray<T: Mappable>(type: T.Type) -> Observable<[T]> {
//        return self.map { response in
//            //if response array, use ObjectMapper to map the array
//            //if not, throw an error
//            guard let array = response as? [Any] else {
//                throw RxSwiftMoyaError.ParseJSONError
//            }
//
//            guard let dicts = array as? [[String: Any]] else {
//                throw RxSwiftMoyaError.ParseJSONError
//            }
//            return Mapper<T>().mapArray(JSONArray: dicts)
//        }
//    }
//
//}
