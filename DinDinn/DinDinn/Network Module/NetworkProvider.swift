//
//  NetworkProvider.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 07/04/21.
//

import Moya
import RxSwift

struct NetworkProvider {
    
    typealias OffersCompletion = (MerchantOffers?, Error?) -> Void
    typealias MenuDetailsCompletion = (MerchantMenu?, Error?) -> Void
    
    private let provider = MoyaProvider<APIURLs>(stubClosure:MoyaProvider.immediatelyStub)
    
    let disposeBag = DisposeBag()
    
    func getOffers(for merchant: String, _ completion: @escaping OffersCompletion) {
        provider.request(.getOffers(merchant)) { result in
            switch result {
            case let .success(result):
                let offers = MerchantOffers(JSON: try! result.mapJSON() as! [String: Any], context: .none)
                completion(offers, nil)
                return
            case let .failure(error):
                print("Error - \(error)")
                completion(nil, error)
                return
            }
        }
    }
    
    func getMenu(forMerchant id: String, _ completion: @escaping MenuDetailsCompletion) {
        provider.request(.getFoodCategories(id)) { result in
            switch result {
            case let .success(result):
                let menuDetails = MerchantMenu(JSON: try! result.mapJSON() as! [String: Any], context: .none)
                completion(menuDetails, nil)
                return
            case let .failure(error):
                print("Error - \(error)")
                completion(nil, error)
                return
            }
        }
    }
}
