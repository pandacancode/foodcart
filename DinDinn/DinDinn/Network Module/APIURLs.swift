//
//  APIURLs.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 07/04/21.
//

import Moya

let MERCHANT_OFFERS_SAMPLE = "{\"offers\":[{\"offerId\":\"id-0\",\"offerTitle\":\"title-0\",\"offerShortText\":\"short-0\",\"offerDescription\":\"description-0\",\"offerThumnailUrl\":\"thumburl-0\",\"offerImageUrl\":\"imageurl-0\"},{\"offerId\":\"id-1\",\"offerTitle\":\"title-1\",\"offerShortText\":\"short-1\",\"offerDescription\":\"description-1\",\"offerThumnailUrl\":\"thumburl-1\",\"offerImageUrl\":\"imageurl-1\"},{\"offerId\":\"id-2\",\"offerTitle\":\"title-2\",\"offerShortText\":\"short-2\",\"offerDescription\":\"description-2\",\"offerThumnailUrl\":\"thumburl-2\",\"offerImageUrl\":\"imageurl-2\"}],\"totalOffers\":3}"

let MENU_SAMPLE = "{\"restaurantName\":\"DinDinn Cafe\",\"totalCategories\":5,\"categories\":[{\"categoryId\":\"cid-0\",\"name\":\"Pizza\",\"totalItems\":3,\"items\":[{\"itemId\":\"cid0-item0\",\"itemName\":\"Pepperoni\",\"itemDesc\":\"Pepperoni, Cheese, Homemade Sauce, Hand tossed\",\"itemSizeWeightDesc\":\"35cms, 120gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid0-item1\",\"itemName\":\"Double Cheese\",\"itemDesc\":\"Mozzerella Cheese, Cheddar, Homemade Sauce, Hand tossed, Wood burnt\",\"itemSizeWeightDesc\":\"35cms, 100gms\",\"itemPrice\":\"28\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid0-item2\",\"itemName\":\"Veggie Lovers\",\"itemDesc\":\"Capsicum, Olives, Onions, Cheese, Homemade Sauce, Hand tossed\",\"itemSizeWeightDesc\":\"35cms, 120gms\",\"itemPrice\":\"30\",\"itemImageURL\":\"imageUrl\"}]},{\"categoryId\":\"cid-1\",\"name\":\"Pasta\",\"totalItems\":3,\"items\":[{\"itemId\":\"cid1-item0\",\"itemName\":\"Aglio-e-olio\",\"itemDesc\":\"Spaghetti, Olive Oil, Black Pepper, Olives\",\"itemSizeWeightDesc\":\"100gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid1-item1\",\"itemName\":\"Aglio-e-olio - 1\",\"itemDesc\":\"Spaghetti, Olive Oil, Black Pepper, Olives\",\"itemSizeWeightDesc\":\"100gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid1-item2\",\"itemName\":\"Aglio-e-olio - 2\",\"itemDesc\":\"Spaghetti, Olive Oil, Black Pepper, Olives\",\"itemSizeWeightDesc\":\"100gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"}]},{\"categoryId\":\"cid-2\",\"name\":\"Sushi\",\"totalItems\":3,\"items\":[{\"itemId\":\"cid2-item0\",\"itemName\":\"Sushi - 1\",\"itemDesc\":\"Sushi sushi sushi\",\"itemSizeWeightDesc\":\"20 pieces, 120gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid2-item1\",\"itemName\":\"Sushi - 2\",\"itemDesc\":\"Sushi sushi sushi\",\"itemSizeWeightDesc\":\"20 pieces, 120gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid2-item2\",\"itemName\":\"Sushi - 3\",\"itemDesc\":\"Sushi sushi sushi\",\"itemSizeWeightDesc\":\"20 pieces, 120gms\",\"itemPrice\":\"32\",\"itemImageURL\":\"imageUrl\"}]},{\"categoryId\":\"cid-3\",\"name\":\"Desserts\",\"totalItems\":3,\"items\":[{\"itemId\":\"cid3-item0\",\"itemName\":\"Cheesecake\",\"itemDesc\":\"Cheese, Cake, Banana\",\"itemSizeWeightDesc\":\"70gms\",\"itemPrice\":\"30\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid3-item1\",\"itemName\":\"Cheesecake-1\",\"itemDesc\":\"Cheese, Cake, Banana\",\"itemSizeWeightDesc\":\"70gms\",\"itemPrice\":\"30\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid3-item2\",\"itemName\":\"Cheesecake-2\",\"itemDesc\":\"Cheese, Cake, Banana\",\"itemSizeWeightDesc\":\"70gms\",\"itemPrice\":\"30\",\"itemImageURL\":\"imageUrl\"}]},{\"categoryId\":\"cid-4\",\"name\":\"Beverages\",\"totalItems\":3,\"items\":[{\"itemId\":\"cid4-item0\",\"itemName\":\"Beverage-1\",\"itemDesc\":\"Classic beverage\",\"itemSizeWeightDesc\":\"120ml\",\"itemPrice\":\"15\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid4-item1\",\"itemName\":\"Beverage-2\",\"itemDesc\":\"Classic beverage\",\"itemSizeWeightDesc\":\"120ml\",\"itemPrice\":\"15\",\"itemImageURL\":\"imageUrl\"},{\"itemId\":\"cid4-item2\",\"itemName\":\"Beverage-3\",\"itemDesc\":\"Classic beverage\",\"itemSizeWeightDesc\":\"120ml\",\"itemPrice\":\"15\",\"itemImageURL\":\"imageUrl\"}]}]}"

enum APIURLs {
    case getOffers(String) // Get merchant-specific offers when the merchant is selected
    case getFoodCategories(String) // Get food categories for merchant, e.g - pizza, sushi, drinks, desserts
}

/*
 Setting up BaseURL and methods taking into consideration that they are merchant specific calls.
 e.g. - Getting offers and food categories for a merchant -
 - https://dindinn.com/yummyFood/api/1234/offers
 - https://dindinn.com/yummyFood/api/1234/categories
 */
extension APIURLs: TargetType {
    var baseURL: URL {
        return URL.init(string: "https://dindinn.com/yummyFood/api/")!
    }
    
    var path: String {
        switch self {
        case .getOffers(let merchantId):
            return "\(merchantId)/offers"
        case .getFoodCategories(let merchantId):
            return "\(merchantId)/categories"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        switch self {
        case .getOffers(_):
            return MERCHANT_OFFERS_SAMPLE.data(using: .utf8)!
        case .getFoodCategories(_):
            return MENU_SAMPLE.data(using: .utf8)!
        }
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
