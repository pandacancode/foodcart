//
//  Router.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 09/04/21.
//

import UIKit

class Router: NSObject {
    func initialViewController() -> MerchantMenuView {
        let interactor = MenuInteractor()
        let presenter = MenuPresenter(interactor: interactor)
        let viewController = MerchantMenuView.instance(withPresenter: presenter)
        return viewController
    }
    
    func moveToCartView() {
        // This router method will be used to navigate the screen to final Cart View
    }
}
