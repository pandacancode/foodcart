//
//  MenuInteractor.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 08/04/21.
//

import RxSwift

class MenuInteractor {
    typealias FetchOffersCompletion = (MerchantOffers?, Error?) -> Void
    typealias FetchMenuDetailsCompletion = (MerchantMenu?, Error?) -> Void
    
    let networkProvider = NetworkProvider()
    
    func getMerchantOffers(withMerchantID id: String, completion: @escaping FetchOffersCompletion) {
        networkProvider.getOffers(for: id) { (offers, error) in
            if error != nil {
                completion(nil, error)
                return
            } else {
                //print("Offers - \(offers)")
                completion(offers, nil)
            }
        }
    }
    
    func getMerchantMenu(withMerchantID id: String, completion: @escaping FetchMenuDetailsCompletion) {
        networkProvider.getMenu(forMerchant: id, { (menu, error) in
            if error != nil {
                completion(nil, error)
                return
            } else {
                //print("Menu details - \(menu)")
                completion(menu, nil)
            }
        })
    }
}
