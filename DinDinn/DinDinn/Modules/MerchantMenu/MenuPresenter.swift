//
//  MenuPresenter.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 08/04/21.
//

import RxSwift

class MenuPresenter {
    private let interactor: MenuInteractor
    private var merchantOffers: MerchantOffers?
    private var merchantMenu: MerchantMenu?
    
    public var offersContentUpdated = PublishSubject<Void>()
    public var menuContentUpdated = PublishSubject<Void>()
    public var cartUpdated = PublishSubject<NSInteger>()
    
    init(interactor: MenuInteractor) {
        self.interactor = interactor
    }
        
    // MARK: - Offer Helper and Getters
    func getMerchantOffers(withMerchantID id:String) {
        interactor.getMerchantOffers(withMerchantID: id, completion: { [weak self] (offers, error) in
            if error == nil {
                guard let strongself = self else { return }
                DispatchQueue.main.async {
                    strongself.merchantOffers = offers
                    strongself.offersContentUpdated.onNext(())
                }
            }
        })
    }
    
    func getTotalOffers() -> Int {
        guard let offers = merchantOffers else {
            return 0
        }
        return offers.offersCount!
    }
    
    // MARK: - Menu Helper and Getters
    func getMenuDetails(withMerchantID id:String) {
        interactor.getMerchantMenu(withMerchantID: id) { [weak self] (menu, error) in
            if error == nil {
                guard let strongself = self else { return }
                DispatchQueue.main.async {
                    strongself.merchantMenu = menu
                    strongself.menuContentUpdated.onNext(())
                }
            }
        }
    }
    
    func getTotalMenuCategories() -> Int {
        return merchantMenu!.totalCategories!
    }
    
    func getCategoryName(atIndexPath i:Int) -> String {
        return merchantMenu!.categories![i].name!
    }
    
    func getCategoryItems(atIndexPath i:Int) -> [Item] {
        return merchantMenu!.categories![i].items!
    }
    
    func addItemToCart(_ obj:Item) {
        let cartObject = CartItems.sharedCart
        cartObject.addItemToCart(withId: obj.itemId!, price: Double(obj.itemPrice!)!)
        cartUpdated.onNext(cartObject.cartItemIds!.count)
    }
}
