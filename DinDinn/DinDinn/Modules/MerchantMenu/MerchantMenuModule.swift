//
//  MerchantMenuModule.swift
//  LinguisticNative
//
//  Created by Ankit Bharadwaj on 08/04/21.
//  
//

import Foundation
import UIKit

// MARK: - Router -> Presenter

protocol MerchantMenuRouterPresenterInterface: RouterPresenterInterface {}

// MARK: - Presenter -> Router

protocol MerchantMenuPresenterRouterInterface: PresenterRouterInterface {}

// MARK: - Presenter -> Interactor

protocol MerchantMenuPresenterInteractorInterface: PresenterInteractorInterface {}

// MARK: - Presenter -> View

protocol MerchantMenuPresenterViewInterface: PresenterViewInterface {
    func viewDidLoad()
}

// MARK: - Interactor -> Presenter

protocol MerchantMenuInteractorPresenterInterface: InteractorPresenterInterface {}

// MARK: - Module Builder

final class MerchantMenuModule: ModuleInterface {
    typealias View = MerchantMenuView
    typealias Presenter = MerchantMenuPresenter
    typealias Router = MerchantMenuRouter
    typealias Interactor = MerchantMenuInteractor

    static func build() -> UIViewController {
        let presenter = Presenter()
        let interactor = Interactor()
        let router = Router()

        let viewModel = MerchantMenuViewModel()
        let view = View()

        view.presenter = presenter
        view.viewModel = viewModel

        presenter.viewModel = viewModel

        assemble(
            presenter: presenter,
            router: router,
            interactor: interactor
        )

        return view
    }
}
