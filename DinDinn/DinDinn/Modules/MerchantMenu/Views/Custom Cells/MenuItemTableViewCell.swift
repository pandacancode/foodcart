//
//  MenuItemTableViewCell.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 10/04/21.
//

import UIKit

let MenuItemTableViewCellID = "MenuItemTableViewCell"

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodTitle: UILabel!
    @IBOutlet weak var foodDetails: UILabel!
    @IBOutlet weak var foodQuantity: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!

    @IBAction func priceButtonClicked(_ sender: UIButton) {
        buttonClickAction?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
        
    var buttonClickAction: (()->())?
}
