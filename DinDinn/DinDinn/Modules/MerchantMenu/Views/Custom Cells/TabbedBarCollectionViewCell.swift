//
//  TabbedBarCollectionViewCell.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 10/04/21.
//

import UIKit

let TabbedBarCollectionViewCellID = "TabbedBarCollectionViewCell"

class TabbedBarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tabNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
