//
//  MerchantMenuView.swift
//  LinguisticNative
//
//  Created by Ankit Bharadwaj on 08/04/21.
//  
//

import Foundation
import UIKit
import RxSwift
import SYBadgeButton

var topViewInitialHeight : CGFloat = 551
let topViewFinalHeight : CGFloat = (UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.size.height)! + 44
let topViewHeightConstraintRange = topViewFinalHeight..<topViewInitialHeight

enum PageViewType {
    case OFFERS_PAGE
    case TABS_PAGE
}

let MERCHANT_ID = "1234"

class MerchantMenuView: UIViewController {
    
    // TOP VIEW
    @IBOutlet weak var offersView: UIView!
    var offersPageViewController = UIPageViewController()
    
    // MIDDLE VIEW
    @IBOutlet weak var tabBarCollectionView: UICollectionView!
    var selectedTabView = UIView()
    
    // BOTTOM VIEW
    @IBOutlet weak var foodCategoriesView: UIView!
    
    @IBOutlet weak var cartButton: SYBadgeButton!
    @IBAction func cartButtonPressed(_ sender: UIButton) {
        let router = Router()
        router.moveToCartView()
    }
    
    var pageViewController = UIPageViewController()
    var foodCategoryCollection = FoodCategoryCollection()
    var offerCategoryCollection = OfferCategoriesCollection()
    private var pageTypeInProgress: PageViewType!
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    // RxSWIFT
    private let disposeBag = DisposeBag()
    private var _presenter: MenuPresenter?
    private var presenter: MenuPresenter {
        guard let presenter = _presenter else { fatalError("presenter not initialized") }
        return presenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindPresenter()
        presenter.getMerchantOffers(withMerchantID: MERCHANT_ID)
        presenter.getMenuDetails(withMerchantID: MERCHANT_ID)
    }
    
    // TopViewMoved method for PanGesture
    var dragInitialY: CGFloat = 0
    var dragPreviousY: CGFloat = 0
    var dragDirection: DragDirection = .Up
    
    @objc func topViewMoved(_ gesture: UIPanGestureRecognizer) {
        var dragYDiff : CGFloat
        switch gesture.state {
        
        case .began:
            dragInitialY = gesture.location(in: self.view).y
            dragPreviousY = dragInitialY
            
        case .changed:
            let dragCurrentY = gesture.location(in: self.view).y
            dragYDiff = dragPreviousY - dragCurrentY
            dragPreviousY = dragCurrentY
            dragDirection = dragYDiff < 0 ? .Down : .Up
            innerTableViewDidScroll(withDistance: dragYDiff)
            
        case .ended:
            innerTableViewScrollEnded(withScrollDirection: dragDirection)
            
        default: return
        }
    }
    
    func setBottomPagingView(toPageWithAtIndex index: Int, andNavigationDirection navigationDirection: UIPageViewController.NavigationDirection) {
        pageViewController.setViewControllers([foodCategoryCollection.categoryCollection[index].vc],
                                                  direction: navigationDirection,
                                                  animated: true,
                                                  completion: nil)
    }
    
    func scrollSelectedTabView(toIndexPath indexPath: IndexPath, shouldAnimate: Bool = true) {
        UIView.animate(withDuration: 0.3) {
            if let cell = self.tabBarCollectionView.cellForItem(at: indexPath) {
                self.selectedTabView.frame.size.width = cell.frame.width
                self.selectedTabView.frame.origin.x = cell.frame.origin.x
            }
        }
    }
}

// MARK: - VIPER Extension
extension MerchantMenuView {
    public static func instance(withPresenter presenter: MenuPresenter) -> MerchantMenuView {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(identifier: "MerchantMenuView") as? MerchantMenuView else {
            fatalError()
        }
        viewController._presenter = presenter
        return viewController
    }
    
    func bindPresenter() {
        presenter.offersContentUpdated
            .subscribe(onNext: { _ in
                self.initializeOffersView()
            }).disposed(by: disposeBag)
        
        presenter.menuContentUpdated
            .subscribe(onNext: { _ in
                self.initializeMenuCategories()
            }).disposed(by: disposeBag)
        
        presenter.cartUpdated
            .subscribe(onNext: { itemsInCart in
                self.updateCartButton(with: itemsInCart)
            }).disposed(by: disposeBag)
    }
}

// MARK: - View Setup Methods
extension MerchantMenuView {
    func initializeOffersView()
    {
        #warning("Handle 0 offers case here..")
        populateOffersView(withTotalOffers: presenter.getTotalOffers())
        setupPagingViewController(with: .OFFERS_PAGE)
    }
    
    func initializeMenuCategories() {
        setupCollectionView()
        setupPagingViewController(with: .TABS_PAGE)
        populateBottomView()
        addPanGestureToTopViewAndCollectionView()
    }
}

// MARK: - Helper - OFFERS VIEW
extension MerchantMenuView {
    func populateOffersView(withTotalOffers count: Int) {
        for _ in 1...count {
            let tabContentVC = OffersViewController()
            let page = OfferCategories(with: tabContentVC)
            offerCategoryCollection.categoryCollection.append(page)
        }
        
        let initialPage = 0
        offersPageViewController.setViewControllers([offerCategoryCollection.categoryCollection[initialPage].vc],
                                                  direction: .forward,
                                                  animated: true,
                                                  completion: nil)
        
        addChild(offersPageViewController)
        offersPageViewController.willMove(toParent: self)
        offersView.addSubview(offersPageViewController.view)
        
        pinOffersViewControllerToTopView()
    }
    
    func pinOffersViewControllerToTopView() {
        offersView.translatesAutoresizingMaskIntoConstraints = false
        offersPageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        offersPageViewController.view.leadingAnchor.constraint(equalTo: offersView.leadingAnchor).isActive = true
        offersPageViewController.view.trailingAnchor.constraint(equalTo: offersView.trailingAnchor).isActive = true
        offersPageViewController.view.topAnchor.constraint(equalTo: offersView.topAnchor).isActive = true
        offersPageViewController.view.bottomAnchor.constraint(equalTo: offersView.bottomAnchor).isActive = true
    }
}

// MARK: - MenuTabs: View & Delegates
extension MerchantMenuView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func setupCollectionView() {
        let layout = tabBarCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.estimatedItemSize = CGSize(width: 100, height: 50)
        
        tabBarCollectionView.register(UINib(nibName: TabbedBarCollectionViewCellID, bundle: nil),
                                      forCellWithReuseIdentifier: TabbedBarCollectionViewCellID)
        tabBarCollectionView.dataSource = self
        tabBarCollectionView.delegate = self
        
        setupSelectedTabView()
    }
    
    func setupSelectedTabView() {
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 10))
        label.text = "TAB \(1)"
        label.sizeToFit()
        var width = label.intrinsicContentSize.width
        width = width + 40
        
        selectedTabView.frame = CGRect(x: 20, y: 55, width: width, height: 5)
        selectedTabView.backgroundColor = UIColor(red:0.65, green:0.58, blue:0.94, alpha:1)
        tabBarCollectionView.addSubview(selectedTabView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getTotalMenuCategories()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let tabCell = collectionView.dequeueReusableCell(withReuseIdentifier: TabbedBarCollectionViewCellID, for: indexPath) as? TabbedBarCollectionViewCell {
            tabCell.tabNameLabel.text = presenter.getCategoryName(atIndexPath: indexPath.row)
            return tabCell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == foodCategoryCollection.selectedPageIndex {
            return
        }
        
        var direction: UIPageViewController.NavigationDirection
        if indexPath.item > foodCategoryCollection.selectedPageIndex {
            direction = .forward
        } else {
            direction = .reverse
        }
        
        foodCategoryCollection.selectedPageIndex = indexPath.item
        tabBarCollectionView.scrollToItem(at: indexPath,
                                          at: .centeredHorizontally,
                                          animated: true)
        scrollSelectedTabView(toIndexPath: indexPath)
        setBottomPagingView(toPageWithAtIndex: indexPath.item, andNavigationDirection: direction)
    }
}

// MARK: - MenuItems - PageViewController
extension MerchantMenuView: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func setupPagingViewController(with type: PageViewType) {
        if type == .OFFERS_PAGE {
            pageTypeInProgress = .OFFERS_PAGE
            offersPageViewController = UIPageViewController(transitionStyle: .scroll,
                                                          navigationOrientation: .horizontal,
                                                          options: nil)
            offersPageViewController.dataSource = self
            offersPageViewController.delegate = self
        } else {
            pageTypeInProgress = .TABS_PAGE
            pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                          navigationOrientation: .horizontal,
                                                          options: nil)
            pageViewController.dataSource = self
            pageViewController.delegate = self
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if pageViewController.isEqual(self.pageViewController) {
            if let currentViewControllerIndex = foodCategoryCollection.categoryCollection.firstIndex(where: { $0.vc == viewController }) {
                if (1..<foodCategoryCollection.categoryCollection.count).contains(currentViewControllerIndex) {
                    // go to previous page in array
                    return foodCategoryCollection.categoryCollection[currentViewControllerIndex - 1].vc
                }
            }
        } else {
            if let currentViewControllerIndex = offerCategoryCollection.categoryCollection.firstIndex(where: { $0.vc == viewController }) {
                if (1..<foodCategoryCollection.categoryCollection.count).contains(currentViewControllerIndex) {
                    // go to previous page in array
                    return offerCategoryCollection.categoryCollection[currentViewControllerIndex - 1].vc
                }
            }
        }
        
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if pageViewController.isEqual(self.pageViewController) {
            if let currentViewControllerIndex = foodCategoryCollection.categoryCollection.firstIndex(where: { $0.vc == viewController }) {
                if (0..<(foodCategoryCollection.categoryCollection.count - 1)).contains(currentViewControllerIndex) {
                    // go to next page in array
                    return foodCategoryCollection.categoryCollection[currentViewControllerIndex + 1].vc
                }
            }
        } else {
            if let currentViewControllerIndex = offerCategoryCollection.categoryCollection.firstIndex(where: { $0.vc == viewController }) {
                if (1..<foodCategoryCollection.categoryCollection.count).contains(currentViewControllerIndex) {
                    // go to previous page in array
                    return offerCategoryCollection.categoryCollection[currentViewControllerIndex - 1].vc
                }
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        
        guard let currentVC = pageViewController.viewControllers?.first else { return }
        guard let currentVCIndex = foodCategoryCollection.categoryCollection.firstIndex(where: { $0.vc == currentVC }) else { return }
        
        let indexPathAtCollectionView = IndexPath(item: currentVCIndex, section: 0)
        
        scrollSelectedTabView(toIndexPath: indexPathAtCollectionView)
        tabBarCollectionView.scrollToItem(at: indexPathAtCollectionView,
                                          at: .centeredHorizontally,
                                          animated: true)
    }
    
    func populateBottomView() {
        for i in 0..<presenter.getTotalMenuCategories() {
            let tabContentVC = MenuItemsViewController()
            tabContentVC.assignPresenter(presenter, andCategoryItems: presenter.getCategoryItems(atIndexPath: i))
            tabContentVC.innerTableViewScrollDelegate = self
            
            let page = FoodCategory(with: tabContentVC)
            foodCategoryCollection.categoryCollection.append(page)
        }
        
        let initialPage = 0
        pageViewController.setViewControllers([foodCategoryCollection.categoryCollection[initialPage].vc],
                                                  direction: .forward,
                                                  animated: true,
                                                  completion: nil)
        
        addChild(pageViewController)
        pageViewController.willMove(toParent: self)
        foodCategoriesView.addSubview(pageViewController.view)
        
        pinPagingViewControllerToBottomView()
    }
    
    func pinPagingViewControllerToBottomView() {
        foodCategoriesView.translatesAutoresizingMaskIntoConstraints = false
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        pageViewController.view.leadingAnchor.constraint(equalTo: foodCategoriesView.leadingAnchor).isActive = true
        pageViewController.view.trailingAnchor.constraint(equalTo: foodCategoriesView.trailingAnchor).isActive = true
        pageViewController.view.topAnchor.constraint(equalTo: foodCategoriesView.topAnchor).isActive = true
        pageViewController.view.bottomAnchor.constraint(equalTo: foodCategoriesView.bottomAnchor).isActive = true
    }
    
    func addPanGestureToTopViewAndCollectionView() {
        let topViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewMoved))
        
        offersView.isUserInteractionEnabled = true
        offersView.addGestureRecognizer(topViewPanGesture)
        
        /* Adding pan gesture to collection view is overriding the collection view scroll.
         
        let collViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewMoved))
        
        tabBarCollectionView.isUserInteractionEnabled = true
        tabBarCollectionView.addGestureRecognizer(collViewPanGesture)
         
        */
    }
}

extension MerchantMenuView {
    func updateCartButton(with itemsCount: Int) {
        cartButton.badgeValue = String(itemsCount)
    }
    
    
}

extension MerchantMenuView: InnerTableViewScrollDelegate {
    
    var currentHeaderHeight: CGFloat {
        return headerViewHeightConstraint.constant
    }
    
    func innerTableViewDidScroll(withDistance scrollDistance: CGFloat) {
        headerViewHeightConstraint.constant -= scrollDistance
        
        /* Don't restrict the downward scroll.
        if headerViewHeightConstraint.constant > topViewInitialHeight {
            headerViewHeightConstraint.constant = topViewInitialHeight
        }
        */
        
        if headerViewHeightConstraint.constant < topViewFinalHeight {
            headerViewHeightConstraint.constant = topViewFinalHeight
        }
    }
    
    func innerTableViewScrollEnded(withScrollDirection scrollDirection: DragDirection) {
        
        let topViewHeight = headerViewHeightConstraint.constant
        
        /*
         *  Scroll is not restricted.
         *  So this check might cause the view to get stuck in the header height is greater than initial height.
 
        if topViewHeight >= topViewInitialHeight || topViewHeight <= topViewFinalHeight { return }
         
        */
        
        if topViewHeight <= topViewFinalHeight + 20 {
            scrollToFinalView()
        } else if topViewHeight <= topViewInitialHeight - 20 {
            switch scrollDirection {
                
            case .Down: scrollToInitialView()
            case .Up: scrollToFinalView()
            
            }
        } else {
            scrollToInitialView()
        }
    }
    
    func scrollToInitialView() {
        
        let topViewCurrentHeight = offersView.frame.height
        let distanceToBeMoved = abs(topViewCurrentHeight - topViewInitialHeight)
        
        var time = distanceToBeMoved / 500
        if time < 0.25 {
            time = 0.25
        }
        
        headerViewHeightConstraint.constant = topViewInitialHeight
        UIView.animate(withDuration: TimeInterval(time), animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollToFinalView() {
        let topViewCurrentHeight = offersView.frame.height
        let distanceToBeMoved = abs(topViewCurrentHeight - topViewFinalHeight)

        var time = distanceToBeMoved / 500
        if time < 0.25 {
            time = 0.25
        }
        
        headerViewHeightConstraint.constant = topViewFinalHeight
        UIView.animate(withDuration: TimeInterval(time), animations: {
            self.view.layoutIfNeeded()
        })
    }
}
