//
//  RequestConversationPresenter.swift
//  LinguisticNative
//
//  Created by Ankit Bharadwaj on 08/04/21.
//  
//

import Foundation
import RxSwift

final class MerchantMenuPresenter: PresenterInterface {
    var router: MerchantMenuRouterPresenterInterface!
    //var interactor: MerchantMenuInteractorPresenterInterface!
    
    var interactor: MerchantMenuInteractor!
    weak var viewModel: MerchantMenuViewModel!
    
    private let disposeBag = DisposeBag()
}

// MARK: - Presenter -> Router

extension MerchantMenuPresenter: MerchantMenuPresenterRouterInterface {}

// MARK: - Presenter -> Interactor

extension MerchantMenuPresenter: MerchantMenuPresenterInteractorInterface {}

// MARK: - Presenter -> View

extension MerchantMenuPresenter: MerchantMenuPresenterViewInterface {
    func viewDidLoad() {
        interactor.getMerchantOffers()
            .subscribe(onNext: { [unowned self] offers in
                self.viewModel.merchantOffers = offers
                print("OffersCount - \(offers.offersCount!)")
            }).disposed(by: disposeBag)
    }
}
