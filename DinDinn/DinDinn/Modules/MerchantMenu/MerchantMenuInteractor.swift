//
//  MerchantMenuInteractor.swift
//  LinguisticNative
//
//  Created by Ankit Bharadwaj on 08/04/21.
//  
//

import Foundation
import RxSwift

final class MerchantMenuInteractor: InteractorInterface {
    weak var presenter: MerchantMenuPresenterInteractorInterface!
    let networkProvider = NetworkProvider()
}

// MARK: - Interactor -> Presenter

extension MerchantMenuInteractor: MerchantMenuInteractorPresenterInterface {
    func getMerchantOffers() -> Observable<MerchantOffers> {
        return networkProvider.getOffers(for: "1234")
    }
}
