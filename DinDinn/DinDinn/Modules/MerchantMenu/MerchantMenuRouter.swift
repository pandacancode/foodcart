//
//  MerchantMenuRouter.swift
//  LinguisticNative
//
//  Created by Ankit Bharadwaj on 08/04/21.
//  
//

import Foundation

final class MerchantMenuRouter: RouterInterface {
    weak var presenter: MerchantMenuPresenterRouterInterface!
}

extension MerchantMenuRouter: MerchantMenuRouterPresenterInterface {}
