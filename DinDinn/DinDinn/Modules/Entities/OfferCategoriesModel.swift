//
//  OfferCategoriesModel.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 16/04/21.
//

import Foundation
import UIKit

struct OfferCategories {
    var vc = UIViewController()
    
    init(with _vc: UIViewController) {
        vc = _vc
    }
}

struct OfferCategoriesCollection {
    var categoryCollection = [OfferCategories]()
    var selectedPageIndex = 0 //The first page is selected by default in the beginning
}

