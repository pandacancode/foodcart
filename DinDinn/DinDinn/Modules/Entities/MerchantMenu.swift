//
//  MerchantMenu.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 17/04/21.
//

import ObjectMapper

class MerchantMenu: Mappable {
    var restaurantName: String?
    var totalCategories: Int?
    var categories: [Category]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        restaurantName <- map["restaurantName"]
        totalCategories <- map["totalCategories"]
        categories <- map["categories"]
    }
}
