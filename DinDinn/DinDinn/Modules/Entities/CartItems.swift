//
//  CartItems.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 20/04/21.
//

import UIKit

class CartItems: NSObject {
    
    static let sharedCart = CartItems()
    
    var cartItemIds: [String]?
    var itemQuantities: [String: Int]?
    var itemPrices: [String: Double]?
    var totalPrice: Double?

    func addItemToCart(withId id: String, price: Double) {
        if cartItemIds != nil {
            cartItemIds?.append(id)
            itemPrices![id] = price
        } else {
            cartItemIds = [String]()
            itemPrices = [String: Double]()
            cartItemIds?.append(id)
            itemPrices![id] = price
        }
        
        print("Items in cart - \(cartItemIds!.count)")
    }
    
    func getTotalCartPrice() {
        for (_, price) in itemPrices! {
            totalPrice! += price
        }
    }
}
