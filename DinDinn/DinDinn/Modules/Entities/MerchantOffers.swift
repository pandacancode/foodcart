//
//  MerchantOffers.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 07/04/21.
//
import ObjectMapper

class MerchantOffers: Mappable {
    var merchantOffers: [MerchantOffer]?
    var offersCount: Int?

    required init?(map: Map) { }

    func mapping(map: Map) {
        merchantOffers <- map["offers"]
        offersCount <- map["totalOffers"]
    }
}
