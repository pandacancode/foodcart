//
//  MerchantOffers.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 07/04/21.
//

import ObjectMapper

class MerchantOffer: Mappable {
    var offerId: String?
    var offerTitle: String?
    var offerShortText: String?
    var offerDescription: String?
    var offerThumnailUrl: String?
    var offerImageUrl: String?

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        offerId <- map["offerId"]
        offerTitle <- map["offerTitle"]
        offerShortText <- map["offerShortText"]
        offerDescription <- map["offerDescription"]
        offerThumnailUrl <- map["offerThumnailUrl"]
        offerImageUrl <- map["offerImageUrl"]
    }
}
