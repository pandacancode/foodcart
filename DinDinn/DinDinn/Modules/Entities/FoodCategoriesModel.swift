//
//  FoodCategoriesModel.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 10/04/21.
//

import Foundation
import UIKit

struct FoodCategory {
    var vc = UIViewController()
    
    init(with _vc: UIViewController) {
        vc = _vc
    }
}

struct FoodCategoryCollection {
    var categoryCollection = [FoodCategory]()
    var selectedPageIndex = 0 //The first page is selected by default in the beginning
}
