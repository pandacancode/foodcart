//
//  Item.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 17/04/21.
//

import ObjectMapper

class Item: Mappable {
    var itemId: String?
    var itemName: String?
    var itemDec: String?
    var itemSizwWeightDesc: String?
    var itemPrice: String?
    var itemImageUrl: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        itemId <- map["itemId"]
        itemName <- map["itemName"]
        itemDec <- map["itemDesc"]
        itemSizwWeightDesc <- map["itemSizeWeightDesc"]
        itemPrice <- map["itemPrice"]
        itemImageUrl <- map["itemImageUrl"]
    }
    
    
}
