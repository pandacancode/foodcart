//
//  Category.swift
//  DinDinn
//
//  Created by Ankit Bharadwaj on 17/04/21.
//

import ObjectMapper

class Category: Mappable {
    var categoryID: String?
    var name: String?
    var totalItems: Int?
    var items: [Item]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        categoryID <- map["categoryId"]
        name <- map["name"]
        totalItems <- map["totalItems"]
        items <- map["items"]
    }
}
